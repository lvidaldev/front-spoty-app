import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  //paises: any[] = [];
  nuevasCanciones: any[] = [];
  loading: boolean;

  constructor( private spotify: SpotifyService ) { 
    /*
    console.log('Constructor del Home hecho');
    this.http.get('https://restcountries.eu/rest/v2/lang/es') //hacemos peticion get
    .subscribe( (resp: any) => { //mantenerme suscrito me permite escuchar la peticion para obener la info apenas se ejecute
      this.paises = resp;
      console.log(resp);
    })*/


    this.loading = true;

    this.spotify.getNewReleases()
    .subscribe( (data : any) =>{
      this.nuevasCanciones = data;
      this.loading = false;
    });
  }


}
