import { MatButtonModule, MatCheckboxModule, MatFormFieldModule } from '@angular/material';
import { Component } from '@angular/core';
import { NgModule, Inject } from '@angular/core';
import { MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';

import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';

import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatSortModule } from '@angular/material/sort';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';


@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatTableModule, MatToolbarModule, MatIconModule, MatSelectModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule, MatListModule, MatPaginatorModule, MatFormFieldModule, MatDialogModule, MatSnackBarModule, MatProgressSpinnerModule, MatCardModule, MatSortModule, MatChipsModule, MatExpansionModule, MatMenuModule],
  exports: [MatButtonModule, MatCheckboxModule, MatTableModule, MatToolbarModule, MatIconModule, MatSelectModule, MatInputModule, MatDatepickerModule, FormsModule, ReactiveFormsModule, MatNativeDateModule, MatListModule, MatPaginatorModule, MatFormFieldModule, MatDialogModule, MatSnackBarModule, MatProgressSpinnerModule, MatCardModule, MatSortModule, MatChipsModule, MatExpansionModule, MatMenuModule],
})
export class MaterialModule { 
}



