import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { 
    console.log('Spotify Service Listo!');
  }

  getQuery( query: string)
  {
    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQD_l8Nwllrun0lER5E-y5hQhvWRqxBF7iTz7u3BnLc882U9QgHY6eQw-GOpmrV7HqvXM7O4rF4o2YPIf3w'
    });

    return this.http.get(url, { headers });
  }

  getNewReleases()
  {
    /*
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQAUmtrI1HDU1gAUkXu0a2VDkBugUDpjO_kZNAkvj7n5sVxau8ITUomMUabATskCHx_NgyM4kwZSQmLDX3A'
    });*/
    return this.getQuery('browse/new-releases?limit=20')
    .pipe( map( data => data['albums'].items));
  }

  getArtistas( termino: string )
  {
    /*
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQAUmtrI1HDU1gAUkXu0a2VDkBugUDpjO_kZNAkvj7n5sVxau8ITUomMUabATskCHx_NgyM4kwZSQmLDX3A'
    });*/
    return this.getQuery(`search?q=${  termino }&type=artist&limit=15`)
    .pipe( map( data => data['artists'].items));

  }

  getArtista( id: string )
  {
    /*
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQAUmtrI1HDU1gAUkXu0a2VDkBugUDpjO_kZNAkvj7n5sVxau8ITUomMUabATskCHx_NgyM4kwZSQmLDX3A'
    });*/
    return this.getQuery(`artists/${ id }`);
    //.pipe( map( data => data['artists'].items));

  }


}
